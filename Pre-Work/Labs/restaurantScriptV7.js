// class name.value.text is empty
// class email.value.text and phone.value.text is empty
// reason.value is other and addInfo.valuetext is empty

// all contactdays.check are false


function isBlank(element) {
	var blank = ["",null,undefined,false];
	var list = document.getElementsByClassName(element);

	for (var i = 0; i < list.length; i++) {
		if (element === "contactDays") {
			var value = list[i].checked;
		} else {
			var value = list[i].value;
		}
		if (blank.indexOf(value) === -1) {
			return false;
		}
	}
	return true;
}

function missing(fields) {
	var list = "";

	for (var i = 0; i < fields.length; i++) {
		list = list + "     * " + fields[i] + "\n";
	}

	return list;
}

function reminder() {
	var fields = [];
	var inquiry = document.getElementById("customerReasons").value;

	if (isBlank("customerName")) {
		fields.push("Full Name");
	}
	if (isBlank("customerEmail") && isBlank("customerPhone")) {
		fields.push("Email or Phone Number");
	}
	if (isBlank("additionalInformation") && inquiry === "other") {
		fields.push("Additional Information");
	}
	if (isBlank("contactDays")) {
		fields.push("Best day(s) to Contact You");
	}
	if (missing.length !== 0) {
		alert(
			"Please enter all required information when contacting us. " +
			"The following required fields are blank and need your attention:\n\n" +
			missing(fields)
		);
	}
}
