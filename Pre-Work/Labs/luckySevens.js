function hideMe() {
	var toHide = document.getElementsByClassName("hide");
	for (var i = 0; i < toHide.length; i++){
		toHide[i].style.display = "none";
	}
}

function show(hidden) {
	hidden.style.display = "inherit";
}

function isValid(bet) {
	if ((bet == parseInt(bet)) && (bet > 0)) {
		hideMe();
		return true;
	} else {
		show(document.getElementById("error-message"));
		return false
	}
}

function dieRoll() {
	return Math.floor(Math.random() * 6) + 1;
}

function isSeven(dieOne,dieTwo,diceRoll) {
	dieOne = dieRoll();
	dieTwo = dieRoll();
	diceRoll = dieOne + dieTwo;

	return diceRoll === 7;
}

function isMaxHeld(bet, maxHeld) {
	return bet > maxHeld;
}

function postResults(startingBet,rollNumber,maxHeld,maxHeldRoll) {
	show(document.getElementById("results"));
	document.getElementById("starting-bet").innerHTML = startingBet;
	document.getElementById("total-rolls").innerHTML = rollNumber;
	document.getElementById("highest-amount").innerHTML = maxHeld;
	document.getElementById("roll-highest").innerHTML = maxHeldRoll;
	document.getElementById("play").value = "Play Again";
}

function playGame(bet) {
	var dieOne,dieTwo,diceRoll,maxHeldRoll;
	var startingBet = bet;
	var rollNumber = 0;
	var maxHeld = bet;
	var maxHeldRoll = 0;


	while (bet > 0) {
		if (isSeven(dieOne,dieTwo,diceRoll)) {
			bet = bet + 4;
		} else {
			bet = bet - 1;
		}
		rollNumber += 1;
		if (isMaxHeld(bet,maxHeld)) {
			maxHeld = bet;
			maxHeldRoll = rollNumber;
		}
	}
	
	postResults(startingBet,rollNumber,maxHeld,maxHeldRoll);
}

function luckySevens() {
	var bet = document.getElementById("bet").value;

	if (isValid(bet)) {
		bet = Number(bet);
		playGame(bet);
	}
}
